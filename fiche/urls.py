from django.contrib import admin
from django.urls import path

from fiche.views import *

app_name = 'main'

urlpatterns = [
    path('', Fiche, name='fiche'),
    path('sources', Sources, name='sources'),
    path('diagramme', Diagramme, name='diagramme'),
    path('approfondissement', Approfondissement, name='approfondissement'),
]