from django.shortcuts import render


def Fiche(request):
    return render(request,'main.html')

def Sources(request):
    return render(request,'sources.html')

def Diagramme(request):
    return render(request,'diagramme.html')

def Approfondissement(request):
    return render(request,'approfondissement.html')